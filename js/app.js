//---------------------------
// true if in development mode
var isVerbose = true;
//---------------------------

var loadingMsg = "";

var getConfig = function(host,post_data){
  var d = new Date();
  var n = d.getMilliseconds();
  
  var config =
  {
    timeout : 30000,
    method: 'POST',
    transformRequest: function(obj) {
      var str = [];
      for(var p in obj)
        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
      return str.join("&");
    },
    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    url: host,
    data : post_data
  };
   if(isVerbose) console.log(config);
  return config;
}

angular.module('starter', ['ionic', 'ngCordova', 'starter.controllers','ion-gallery','ionicLazyLoad'])

.run(function($ionicPlatform,$rootScope,$ionicLoading,$cordovaSocialSharing,$cordovaToast,$ionicPopup,$cordovaDevice,$http,$state) {


  $ionicPlatform.registerBackButtonAction(function (event) {
    if (true) { // your check here
      $ionicPopup.confirm({
        title: 'Konfirmasi Keluar',
        template: 'Keluar dari aplikasi ?'
      }).then(function (res) {
        if (res) {
          ionic.Platform.exitApp();
        }
      })
    }
  }, 100);

  $ionicPlatform.ready(function () {


    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      //cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
    if (isVerbose) console.log('device ready !!!');

    $rootScope.currentPlatform = ionic.Platform.platform();

    if (isVerbose) console.log($rootScope.currentPlatform);

    // ios background process listener
    if ($rootScope.currentPlatform == "ios") {
      document.addEventListener("pause", function () {
        $state.go("app.berita");
      }, false);
    }

    try
    {
      $rootScope.deviceId = $cordovaDevice.getUUID();
    }
    catch (ex)
    {
      console.log(ex.message);
    }


  //   // =================================================
  //   // Firebase Cloud Messaging Function
  //   // =================================================

  //   try{
  //     FCMPlugin.getToken(
  //         function (token) {
  //           console.log(token);
  //           alert(token);
  //           $rootScope.fcmToken = token;
  //           $rootScope.sendToPushServer($rootScope.deviceId,$rootScope.fcmToken);

  //         },
  //         function (err) {
  //           console.log('error retrieving token: ' + err);
  //         }
  //     );

  //     FCMPlugin.onNotification(
  //         function(data){
  //           if(data.wasTapped){
  //             //Notification was received on device tray and tapped by the user.
  //             alert( JSON.stringify(data) );
  //           }else{
  //             //Notification was received in foreground. Maybe the user needs to be notified.
  //             alert( JSON.stringify(data) );
  //           }
  //         },
  //         function(msg){
  //           console.log('onNotification callback successfully registered: ' + msg);
  //         },
  //         function(err){
  //           console.log('Error registering onNotification callback: ' + err);
  //         }
  //     );
  //   }
  //   catch (ex)
  //   {
  //     console.log(ex.message);
  //   }

  //   //===========================================
  //   // FCM Function Ends.
  //   //===========================================

   });


   $rootScope.host = "https://api.setneg.go.id";
   $rootScope.urlSetneg = "https://www.setneg.go.id";
   $rootScope.option = {};


  var fromLocal = JSON.parse(window.localStorage.getItem("option"));
  if(isVerbose) console.log("======================");
  if(isVerbose) console.log(fromLocal);
  if(isVerbose) console.log("======================");
  if (!fromLocal)
  {
    $rootScope.option['bahasa'] = "id"; //hardcode
    window.localStorage.setItem("option",JSON.stringify($rootScope.option));
    if(isVerbose) console.log('initial key "option" has been set. ');

  }
  else
  {
    $rootScope.option = fromLocal;
  }

  if($rootScope.option.bahasa == 'id')
  {
    $rootScope.headerTitle = "KEMENTERIAN SEKRETARIAT";
    $rootScope.headerSubtitle = "NEGARA REPUBLIK INDONESIA";

    $rootScope.PHolderSearch = "Saya ingin mengetahui.. ";
    loadingMsg = "Mengambil data..";
  }
  else
  {
    $rootScope.headerTitle = "MINISTRY STATE SECRETARY";
    $rootScope.headerSubtitle = "REPUBLIC OF INDONESIA";

    $rootScope.PHolderSearch = "I want to know.. ";
    loadingMsg = "Loading data..";
  }





  $rootScope.closeApp = function () {
    $ionicPopup.confirm({
      title: 'Konfirmasi Keluar',
      template: 'Keluar dari aplikasi ?'
    }).then(function(res) {
      if (res) {
        ionic.Platform.exitApp();
      }
    });

  }

  $rootScope.sendToPushServer = function(deviceId,token){

  }

  $rootScope.shareAnywhere = function (message,image,link) {
    try {
      if(isVerbose) console.log('init share social...');
      if(isVerbose) console.log(message+"|"+image+"|"+link);
      var loadingMsg = "Mohon tunggu..";
      var promise = $rootScope.showLoading(loadingMsg);
      $cordovaSocialSharing.share(message, "", null, link)
          .then(function (result) {
            if(isVerbose) console.log(result);
            var pr = $rootScope.hideLoading();
          }, function (err) {
            if(isVerbose) console.log(err);
            if(isVerbose) console.log('share FAILED !!');
            $rootScope.toastMessage("Selected application not installed/supported.")
            var pr = $rootScope.hideLoading();
          });
    }
    catch (ex)
    {
      var pr = $rootScope.hideLoading();
      if(isVerbose) console.log("Exception : "+ex.message);
    }
  }

  $rootScope.shareFB = function (message,image,link) {
    if(isVerbose) console.log('init share FB...');
    if(isVerbose) console.log(message+"|"+image+"|"+link);
    var loadingMsg = "Mohon tunggu..";
    try
    {
      var promise = $rootScope.showLoading(loadingMsg);
      $cordovaSocialSharing
          .shareViaFacebook(message, image, link)
          .then(function (result) {
            if(isVerbose) console.log(result);
            var promise = $rootScope.hideLoading();
          }, function (err) {
            if(isVerbose) console.log(err);
            if(isVerbose) console.log('share FAILED !!');
            var promise = $rootScope.hideLoading();
            $rootScope.toastMessage("Facebook application not installed.")
          });
    }
    catch (ex)
    {
     var pr = $rootScope.hideLoading();
      if(isVerbose) console.log("Exception : "+ex.message);
    }
  }

  $rootScope.shareTwitter = function (message,image,link) {
    if(isVerbose) console.log('init share twitter...');
    if(isVerbose) console.log(message+"|"+image+"|"+link);
    try {
      $cordovaSocialSharing
          .shareViaTwitter(message, image, link)
          .then(function (result) {
            if(isVerbose) console.log(result);
          }, function (err) {
            if(isVerbose) console.log(err);
            if(isVerbose) console.log('share FAILED !!');
            $rootScope.toastMessage("Twitter application not installed.")
          });
    }
    catch (ex)
    {
      var pr = $rootScope.hideLoading();
      if(isVerbose) console.log("Exception : "+ex.message);
    }
  }

  $rootScope.toastMessage = function(msg,showTime,posisi){
    if(!showTime)
      showTime = "short";

    if(!posisi)
      posisi = "bottom";

    //toast magic here
    $cordovaToast
        .show(msg, showTime, posisi)
        .then(function(success) {
          // success
        }, function (error) {
          // error
        });
  }

  $rootScope.showLoading = function(msg){
    var finalMsg = msg+"<br><ion-spinner icon='spiral'></ion-spinner>";
    // var finalMsg = "<ion-spinner icon='spiral'></ion-spinner>";
    var promise = $ionicLoading.show({
      template: finalMsg
    });

    return promise;
  };

  $rootScope.hideLoading = function(){
    var promise = $ionicLoading.hide().then(function(){
      //callback if needed
    });

    return promise;
  }

})

.directive('errSrc', function() {
      return {
        link: function(scope, element, attrs) {
          element.bind('error', function() {
            if (attrs.src != attrs.errSrc) {
              attrs.$set('src', attrs.errSrc);
            }
          });
        }
      }
    })

.directive('ngEnter', function () {
  return function (scope, element, attrs) {
    element.bind("keydown keypress", function (event) {
      if(event.which === 13) {
        scope.$apply(function (){
          scope.$eval(attrs.ngEnter);
        });

        event.preventDefault();
      }
    });
  };
})

.config(function($stateProvider, $urlRouterProvider,ionGalleryConfigProvider,$ionicConfigProvider) {
  $ionicConfigProvider.navBar.alignTitle('center');

  $stateProvider
    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })

  .state('app.berita', {
    cache: false,
    url: '/berita',
    views: {
      'beritaContent': {
        templateUrl: 'templates/berita.html',
        controller: 'BeritaCtrl'
      }
    }
  })

  .state('app.galeri', {
    cache: false,
    url: '/galeri',
      views: {
        'galeriContent': {
          templateUrl: 'templates/galeri.html',
          controller: 'GaleriCtrl'
        }
      }
    })
    .state('app.info', {
      cache: false,
      url: '/info',
      views: {
        'infoContent': {
          templateUrl: 'templates/info.html',
            controller: 'InfoCtrl'
        }
      }
    })

  .state('app.pengaturan', {
    cache: false,
    url: '/pengaturan',
      views: {
        'pengaturanContent': {
          templateUrl: 'templates/pengaturan.html',
          controller: 'PengaturanCtrl'
        }
      }
  })
      .state('app.sharer', {
        cache: false,
        url: '/sharer',
        views: {
          'sharerContent': {
            templateUrl: 'templates/sharer.html',
            controller: 'SharerCtrl'
          }
        }
      })
      .state('app.tautan', {
        cache: false,
        url: '/tautan',
        views: {
          'tautanContent': {
            templateUrl: 'templates/tautan.html',
            controller: 'TautanCtrl'
          }
        }
      })
      .state('app.home', {
        cache: false,
        url: '/home',
        views: {
          'homeContent': {
            templateUrl: 'templates/home.html',
            controller: 'HomeCtrl'
          }
        }
      });
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/berita');
  ionGalleryConfigProvider.setGalleryConfig({
    action_label: 'X',
    toggle: false,
    row_size: 3,
    fixed_row_size: true
  });

});
