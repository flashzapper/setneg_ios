angular.module('starter.controllers', [])

    .controller('HomeCtrl', function($scope,$rootScope,$http,$timeout , $state,$ionicPopup , $ionicModal , $stateParams, $interval,$ionicSideMenuDelegate,$ionicTabsDelegate) {
        if(isVerbose) console.log();
        $state.go("app.berita");
    })
    .controller('AppCtrl', function($scope,$rootScope,$http,$timeout , $ionicPopup , $ionicModal , $stateParams, $interval,$ionicSideMenuDelegate,$ionicTabsDelegate) {
})
      .controller('BeritaCtrl', function($sce,$scope,$rootScope,$http,$timeout ,$state, $ionicPopup , $ionicModal , $ionicPopover,$ionicSlideBoxDelegate) {

        $scope.getCustomDate = function(givenDate)
        {
            var editedDate = moment(givenDate).locale("ID").format('DD/MM/YYYY hh:mm A');
            return editedDate;
        }

        $scope.loadData = function(mode,offset,isMore){

            if(isVerbose) console.log(mode+"|"+offset+"|"+isMore);
            var paramData = {};
            // var limit = 20;
            if(!offset)
            {
                offset = 0;
            }

            if(!mode)
            {
                mode = "beritautama";
            }
            $scope.currentOffset = offset;
            $scope.mode = mode;

            paramData['parameter'] = "I";
            paramData['lg'] = $rootScope.option.bahasa;
            // paramData['l'] = limit;
            paramData['o'] = $scope.currentOffset.toString();


            switch($scope.mode)
            {
                case "beritautama" :
                    var host = $rootScope.host+"/berita_utama";break;
                    $scope.isBeritaUtama = true;
                case "KEMENSETNEG" :
                    var host = $rootScope.host+"/berita";break;
                    $scope.isBeritaUtama = false;
                case "presiden" :
                    var host = $rootScope.host+"/berita_presiden";break;
                    $scope.isBeritaUtama = false;
                case "artikel" :
                    var host = $rootScope.host+"/artikel";break;
                    $scope.isBeritaUtama = false;
                case "serba" :
                    var host = $rootScope.host+"/serbaserbi";break;
                    $scope.isBeritaUtama = false;
            }



            var config = getConfig(host,paramData);
            //show loading


            if(!isMore)
            {
                var promise = $rootScope.showLoading(loadingMsg);
            }

             //$http.post(host,paramData, { headers: {'Content-Type': 'application/x-www-form-urlencoded'} } ).then(
            $http(config).then( 
                function(result){
                    if(isVerbose) console.log(result.data.data);
                    if(result.data.data)
                    {
                        if(result.data.data.length <= 0)
                        {
                            $scope.noMoreData = true;
                            if(isVerbose) console.log('no more data');
                        }
                        else
                        {
                            $scope.noMoreData = false;
                            if(isVerbose) console.log('more data to load!');
                        }

                        if(!isMore)
                        {
                            $scope.data = result.data.data;
                            if(isVerbose) console.log('init data loaded!');
                        }
                        else
                        {
                            if(result.data.data.length > 0)
                            {
                                $scope.data = $scope.data.concat(result.data.data);
                                if(isVerbose) console.log('data should be merged.');
                            }
                        }
                    }

                    $scope.config = result.data.config;
                    // $scope.config.cdn = "http://testingcdn.setneg.go.id/";
                    $rootScope.sharer = {};
                    try
                    {
                        $rootScope.sharer['mobile'] = result.data.config.mobile.url.ios;    
                    }
                    catch(ex)
                    {
                        $rootScope.sharer['mobile'] = "";
                    }
                    
                    if(isVerbose) console.log($rootScope.sharer.mobile);

                    if($scope.selected != 'beritautama')
                    {
                        $scope.$broadcast('scroll.infiniteScrollComplete');
                        $scope.$broadcast('scroll.refreshComplete');
                    }
                    else
                    {
                        $ionicSlideBoxDelegate.loop(true);
                        $ionicSlideBoxDelegate.update();
                    }

                    var promise = $rootScope.hideLoading();
            },
                function(error){
                    var msg = "Data gagal diambil.";
                    var promise = $rootScope.hideLoading();
                    promise.then(function(){
                        var alertPopup = $ionicPopup.alert({
                            title: 'Alert',
                            template: msg
                        });
                        alertPopup.then(function(){
                        });
                    });
                });
        }

        $scope.goDetailBerita = function(idBerita,isSearchMode){

            var paramData = {};
            var host = $rootScope.host+"/baca/"+idBerita;
            var config = getConfig(host,paramData);
            //show loading
            var promise = $rootScope.showLoading(loadingMsg);


            $http(config).then(
                function(result) {
                    $scope.detailData = result.data.data;
                    $scope.detailConfig = result.data.config;

                    $scope.dataAttachment = [];

                    if(isVerbose) console.log($scope.detailData);

                    try
                    {
                        for(index in $scope.detailData.media[0].other)
                        {
                            if(typeof $scope.detailData.media[0].other != "undefined")
                            {
                                var temp = {
                                    'mime_type' : $scope.detailData.media[0].other[index].media_mime_type,
                                    'url_media' : $sce.trustAsResourceUrl($scope.detailConfig.cdn + $scope.detailData.media[0].other[index].media_guid),
                                    'url_pdf' : $scope.detailConfig.cdn + $scope.detailData.media[0].other[index].media_guid,
                                    'url_m3u8' :  $sce.trustAsResourceUrl($scope.detailConfig.cdn + $scope.detailData.media[0].other[index].media_m3u8)
                                }

                                $scope.dataAttachment.push(temp);

                            //region region old code
                                // $scope.mimeType = row.media_mime_type;
                                // $scope.urlFoto = $sce.trustAsResourceUrl($scope.detailConfig.cdn + $scope.detailData.media[0].other[0].media_guid);
                                // switch ($rootScope.currentPlatform)
                                // {
                                //     case 'ios' :
                                //         $scope.urlVideo = $sce.trustAsResourceUrl($scope.detailConfig.cdn + $scope.detailData.media[0].other[0].media_m3u8);
                                //         break;
                                //     case 'android' :
                                //         $scope.urlVideo = $sce.trustAsResourceUrl($scope.detailConfig.cdn + $scope.detailData.media[0].other[0].media_guid);
                                //         break;
                                //
                                // }
//endregion
                            }
                            else
                            {
                                if(isVerbose)  console.log('ga punya attachment');
                            }
                        }
                     }
                    catch (ex)
                    {
                        if(isVerbose) console.log("Exception : "+ex.message);
                    }

                    // if(isVerbose) console.log($scope.mimeType+"|"+$scope.urlFoto+"|"+$scope.urlVideo);
                    // if(isVerbose) console.log($scope.detailData.related);



            

                    var re = /([a-z]+\:\/+)([^\/\s]*)([a-z0-9\-@\^=%&;\/~\+]*)[\?]?([^ \#]*)#?([^ \#]*)/ig;
                    var m;
                    var flag = 0;
                    var tempArray = [];

                    while ((m = re.exec($scope.detailData.post_content)) !== null) {
                        if (m.index === re.lastIndex) {
                            re.lastIndex++;
                        }
                        tempArray.push(m);
                    }

                    // console.log(tempArray);

                    var re2 = /(?:\.([^.]+))?$/;
                    var myFilterHtml;
                    for(x in tempArray)
                    {
                        for(y in tempArray[x])
                        {
                            // console.log(tempArray[x][y]);
                            try
                            {
                                var tempText =  tempArray[x][y];
                                tempText =  tempText.replace('"','');
                                var ext = re2.exec(tempText);
                                if(ext[1] == "mp4")
                                {
                                    myFilterHtml = ext['input'];
                                    console.log(myFilterHtml);
                                    flag = 1;
                                    break;
                                }

                            }
                            catch (ex)
                            {}
                        }
                        if(flag == 1) break;
                    }


                    // myFilterHtml = myFilterHtml.replace('.mp4','.m3u8');
                    // console.log(myFilterHtml);
                    // myFilterHtml = $sce.trustAsResourceUrl(myFilterHtml);
                    // $scope.detailData.post_content = '<video controls="true" width="300" height="150"><source src="'+myFilterHtml+'" /></video>';



                    if(isVerbose)  console.log("before");
                    if(isVerbose)  console.log($scope.detailData.post_content);

                    // switch ($rootScope.currentPlatform)
                    // {
                    //     case 'ios' :
                            $scope.detailData.post_content = $scope.detailData.post_content.replace('type="video/mp4"'," ");
                            $scope.detailData.post_content = $scope.detailData.post_content.replace(".mp4",".m3u8");
                    //         $scope.detailData.post_content = $scope.detailData.post_content.replace("src","ng-src");
                    //         $scope.detailData.post_content = $scope.detailData.post_content.replace("source  src","source ng-src");
                    //         $scope.detailData.post_content = $scope.detailData.post_content.replace('crossorigin=""',"");
                    //         $scope.detailData.post_content = $scope.detailData.post_content.replace('class="video-js vjs-default-skin vjs-big-play-centered" ',"");
                            // $scope.detailData.post_content = $scope.detailData.post_content.replace('controls="controls"','controls="true');
                            // break;
                    // }

                   // console.log(result);

                    if(isVerbose)  console.log("after replace");
                    if(isVerbose)  console.log($scope.detailData.post_content);

                    $scope.detailData.post_content = $sce.trustAsHtml($scope.detailData.post_content);

                    if(isVerbose)  console.log("after trusted SCE");
                    if(isVerbose)  console.log($scope.detailData.post_content);
                    if(isVerbose)  console.log($scope.dataAttachment);
                    var promise = $rootScope.hideLoading();
                    if(isSearchMode)
                    {
                        $scope.hideModal();
                        $timeout(function(){
                            $scope.showModal('modalBerita.html');
                        },300);
                    }
                    else
                    {
                        $scope.showModal('modalBerita.html');
                    }
                },
                function(error){
                    var msg = "Data gagal diambil.";
                    var promise = $rootScope.hideLoading();
                    promise.then(function(){
                        var alertPopup = $ionicPopup.alert({
                            title: 'Alert',
                            template: msg
                        });
                        alertPopup.then(function(){
                        });
                    });
                });
        }

        $scope.searchBerita = function(keyword){
           if(!keyword || keyword.length == 0)
            {
                if(ionic.Platform.isIOS())
                {
                    try {
                        if ($rootScope.option.bahasa == "id")
                            $rootScope.toastMessage('Harap masukan kata pencarian.');
                        else
                            $rootScope.toastMessage('Please enter search keyword.');
                    }
                    catch (ex)
                    {
                        if(isVerbose) console.log("Exception : "+ex.message);
                    }
                }
                return;
            }

            var paramData = {};
            paramData['parameter'] = "I";
            paramData['lg'] = $rootScope.option.bahasa;
            
            var host = $rootScope.host+"/search/"+keyword;

            var config = getConfig(host,paramData);
            //show loading
            var promise = $rootScope.showLoading(loadingMsg);


            $http(config).then(
                function(result){
                    $scope.dataSearch = result.data.data;
                    $scope.configSearch = result.data.config;
                    if(isVerbose) console.log($scope.dataSearch);
                    var promise = $rootScope.hideLoading();


                    $scope.isSearch = true;

                    //$scope.$apply(function () {});
                    $scope.showModal('search.html');

                },
                function(error){
                    var msg = "Data gagal diambil.";
                    var promise = $rootScope.hideLoading();
                    promise.then(function(){
                        var alertPopup = $ionicPopup.alert({
                            title: 'Alert',
                            template: msg
                        });
                        alertPopup.then(function(){
                        });
                    });
                });
        }


        $scope.showModal = function(namefile,selectedIndex){
            $ionicModal.fromTemplateUrl('templates/'+namefile, {
                scope: $scope,
                animation: 'slide-in-right'
            }).then(function(modal) {
                $scope.selectedIndex = selectedIndex;
                $scope.modal = modal;
                $scope.modal.show();
            });
        }

        $scope.hideModal = function(){
            $scope.modal.hide();
            $timeout(function(){
                $scope.modal.remove();
                if(isVerbose) console.log('modal destroyed');
                // $scope.keyword = "";
            },300);
            if($scope.popover)
            {
                $scope.popover.hide();
            }
        }
        
        $scope.goState = function (state) {
            $state.go(state);
        }

        $scope.goPdf = function(url){
            console.log(url);
            var trustedUrl = $sce.trustAsResourceUrl(url);
            window.open(trustedUrl, '_blank');
        }

        $scope.setMode = function(item){
            $scope.selected = item;
            if(isVerbose) console.log($scope.selected);
            $scope.$broadcast('scroll.scrollTop');
            $scope.loadData(item);

        }
        $scope.isActive = function(item){
        return $scope.selected === item;
        }

        $scope.optionsSlider = {
            loop: true,
            effect: 'slide',
            autoplay: true,
            speed: 3000
        }

            $scope.checkClass = function(){
            if($scope.selected == 'beritautama')
                return '';
                //return 'beritautamaFill';
        }

        $scope.showPopover = function($event){
            $ionicPopover.fromTemplateUrl('templates/popover.html', {
                scope: $scope,
            }).then(function(popover) {
                $scope.popover = popover;
                $scope.popover.show($event);
            });
        }

          $scope.loadFavorites = function () {
              var paramData = {};
              var host =  $rootScope.host+"/favorites";
              var config = getConfig(host,paramData);
              $scope.favoriteData = "";
              $http(config).then(
                  function(result){
                      $scope.favoriteData = result.data.data.map(function(elem){
                          return elem.name;
                      }).join(" | ");

                      // console.log("=======================");
                      // console.log($scope.favoriteData);
                      // console.log("=======================");
                  }
                  ,function(error){}
              );

          }


        // $scope.$on("$ionicSlides.sliderInitialized", function(event, data){
        //     // data.slider is the instance of Swiper
        //     $scope.slider = data.slider;
        //     if(isVerbose) console.log('Slide initialized');
        // });
        //
        // $scope.$on("$ionicSlides.slideChangeStart", function(event, data){
        //     if(isVerbose) console.log('Slide change is beginning');
        // });
        //
        // $scope.$on("$ionicSlides.slideChangeEnd", function(event, data){
        //     // note: the indexes are 0-based
        //     $scope.activeIndex = data.slider.activeIndex;
        //     $scope.previousIndex = data.slider.previousIndex;
        // });

        $scope.setMode('beritautama');
        $scope.currentOffset = 0;
        $scope.noMoreData = false;
         //init call
        $scope.loadFavorites();



})
    .controller('GaleriCtrl', function($sce,$scope,$rootScope,$http,$timeout , $ionicPopup , $ionicModal , $interval,$ionicSideMenuDelegate,$ionicTabsDelegate) {

        $scope.loadData = function(mode){

            var paramData = {};
            switch(mode)
            {
                case "foto" :
                    var host = $rootScope.host+"/galeri/foto/all";break;
                case "video" :
                    var host = $rootScope.host+"/galeri/video";break;
            }

            var config = getConfig(host,paramData);
            //show loading
            var promise = $rootScope.showLoading(loadingMsg);


            $http(config).then(
                function(result){
                    $scope.data = result.data.data;
                    $scope.config = result.data.config;
                    $scope.items = [];

                    if(isVerbose) console.log($scope.data);
                    if(isVerbose) console.log($scope.data.length);

                    //region -- Old Code region --
                    /*
                    for(index in $scope.data)
                    {
                        for(subIndex in $scope.data[index].media[$scope.data[index].post_id].other)
                        {
                            var tempObj = {};
                            tempObj['src'] = $scope.config.cdn + $scope.data[index].media[$scope.data[index].post_id].other[subIndex].media_guid;
                            tempObj['sub'] = "";
                            $scope.items.push(tempObj);
                        }
                    }
                    */
                    //endregion

                    for(index in $scope.data)
                    {
                        var tempObj = {};
                        tempObj['src'] = $scope.config.cdn + $scope.data[index].media_guid;
                        tempObj['sub'] = $scope.data[index].media_title;
                        $scope.items.push(tempObj);
                    }

                    if(isVerbose) console.log($scope.items);
                    var promise = $rootScope.hideLoading();
                },
                function(error){
                    var msg = "Data gagal diambil.";
                    var promise = $rootScope.hideLoading();
                    promise.then(function(){
                        var alertPopup = $ionicPopup.alert({
                            title: 'Alert',
                            template: msg
                        });
                        alertPopup.then(function(){
                        });
                    });
                });
        }
        $scope.loadDataDetailAlbum = function(id){

            var paramData = {};
            var host = $rootScope.host+"/foto/"+id;

            var config = getConfig(host,paramData);
            //show loading
            var promise = $rootScope.showLoading(loadingMsg);


            $http(config).then(
                function(result){
                    $scope.albumData = result.data.data;
                    $scope.configAlbum = result.data.config;
                    $scope.items = [];

                    var promise = $scope.hideLoading();
                    for(index in $scope.albumData.other)
                    {
                        var tempObj = {};
                        tempObj['src'] = $scope.configAlbum.cdn + $scope.albumData.other[index].media_guid;
                        tempObj['sub'] = "";
                        $scope.items.push(tempObj);
                    }

                    if(isVerbose) console.log($scope.items);
                    $scope.showModal('modalGaleri.html');
                },
                function(error){
                    var msg = "Data gagal diambil.";
                    var promise = $rootScope.hideLoading();
                    promise.then(function(){
                        var alertPopup = $ionicPopup.alert({
                            title: 'Alert',
                            template: msg
                        });
                        alertPopup.then(function(){
                        });
                    });
                });
        }
        $scope.setMode = function(item){
            if(isVerbose) console.log(item);
            $scope.selected = item;
            $scope.loadData(item);
        }
        $scope.isActive = function(item){
           return $scope.selected === item;
        }

        $scope.showModal = function(namefile,selectedIndex){
            $ionicModal.fromTemplateUrl('templates/'+namefile, {
                scope: $scope,
                animation: 'slide-in-right'
            }).then(function(modal) {
                $scope.selectedIndex = selectedIndex;
                $scope.modal = modal;
                $scope.modal.show();
            });
        }

        $scope.hideModal = function(){
            $scope.modal.hide();
        }

        $scope.$on('modal.hidden', function() {
            $timeout(function(){
                if(isVerbose) console.log('modal destroyed');
                $scope.modal.remove();
            },300);

        });

        $scope.goDetail = function (id,posisi) {
            if(isVerbose) console.log(id,posisi);
            switch ($scope.selected)
            {
                case 'foto' :
                    $scope.loadDataDetailAlbum(id,posisi);
                    break;
                case 'video' :
                    if(isVerbose) console.log(id,posisi);
                    var data = $scope.data;
                    var config = $scope.config;

                    $scope.titleVideo = data[posisi].post_title;
                    // $scope.titleVideo = data[posisi].media[id].other[0].media_title;
                    // switch ($rootScope.currentPlatform)
                    // {
                    //     case 'ios' :
                    //         $scope.urlVideo = $sce.trustAsResourceUrl(config.cdn + data[posisi].media[id].other[0].media_m3u8);
                    //         break;
                    //     case 'android' :
                    //         $scope.urlVideo = $sce.trustAsResourceUrl(config.cdn + data[posisi].media[id].other[0].media_guid);
                    // }

                    $scope.urlVideo = $sce.trustAsResourceUrl(config.cdn + data[posisi].media[id].other[0].media_m3u8);
                    // if(isVerbose) console.log(config.cdn + data[posisi].media[id].other[0].media_guid);
                    if(isVerbose) console.log(config.cdn + data[posisi].media[id].other[0].media_m3u8);
                    if(isVerbose) console.log($scope.urlVideo);
                    $scope.showModal('modalGaleri.html');
                    break;
            }

        }
        
        $scope.setMode('foto');
     //   $scope.loadData('foto');



    })
   .controller('PengaturanCtrl', function($scope, $stateParams, $ionicModal, $timeout , $rootScope) {
        $scope.showModal = function (namefile, selectedIndex) {
            $ionicModal.fromTemplateUrl('templates/' + namefile, {
                scope: $scope,
                animation: 'slide-in-right'
            }).then(function (modal) {
                $scope.selectedIndex = selectedIndex;
                $scope.modal = modal;
                $scope.modal.show();
            });
        }
        $scope.hideModal = function () {
            $scope.modal.hide();
        }
        $scope.isActive = function (item) {
            return $scope.selected === item;
        }

        $scope.$watch("option", function(newVal) {
            $rootScope.option = $scope.option;
            window.localStorage.setItem("option",JSON.stringify($rootScope.option));
            if(isVerbose) console.log("updated Key 'option' bahasa to "+newVal.bahasa);
            if(newVal.bahasa == 'id')
            {
                $rootScope.headerTitle = "KEMENTERIAN SEKRETARIAT";
                $rootScope.headerSubtitle = "NEGARA REPUBLIK INDONESIA";
            }
            else
            {
                $rootScope.headerTitle = "MINISTRY STATE SECRETARY";
                $rootScope.headerSubtitle = "REPUBLIC OF INDONESIA";
            }
        },true);

        $scope.option = $rootScope.option;





    })
    .controller('TautanCtrl', function($scope, $stateParams, $ionicModal, $timeout , $rootScope) {
        $scope.showModal = function (namefile, selectedIndex) {
            $ionicModal.fromTemplateUrl('templates/' + namefile, {
                scope: $scope,
                animation: 'slide-in-right'
            }).then(function (modal) {
                $scope.selectedIndex = selectedIndex;
                $scope.modal = modal;
                $scope.modal.show();
            });
        }
        $scope.hideModal = function () {
            $scope.modal.hide();
        }
        $scope.isActive = function (item) {
            return $scope.selected === item;
        }
        $scope.loadData = function(){
            $scope.data = [
                {
                    "url" : $rootScope.urlSetneg+"/formulir_permohonan_informasi",
                    "title" : "Formulir Permohonan Informasi"
                },
                {
                    "url" : $rootScope.urlSetneg+"/formulir_pernyataan_keberatan",
                    "title" : "Formulir Pernyataan Keberatan Atas Permohonan Informasi"
                },
                {
                    "url" : $rootScope.urlSetneg+"/daftar_pengumuman_lelang",
                    "title" : "Daftar Pengumuman Lelang"
                },
                {
                    "url" : $rootScope.urlSetneg,
                    "title" : "Website Setneg"
                },
            ];
        };


        $scope.loadData();


    })
        .controller('SharerCtrl', function($scope, $stateParams, $ionicModal, $timeout , $rootScope) {
        $scope.setMode = function(item){
            $scope.selected = item;
        }

        $scope.isActive = function(item){
            return $scope.selected === item;
        }

    })
    .controller('InfoCtrl', function($sce,$scope,$rootScope,$http,$timeout , $ionicPopup , $ionicModal , $interval,$ionicSideMenuDelegate,$ionicTabsDelegate) {

        $scope.loadData = function(){

            var paramData = {};

            var host = $rootScope.host+"/info_penting";

            var config = getConfig(host,paramData);
            //show loading
             var promise = $rootScope.showLoading(loadingMsg);


            $http(config).then(
                function(result){
                    $scope.data = result.data.data;
                    $scope.config = result.data.config;
                    if(isVerbose) console.log($scope.data);
                    var promise = $rootScope.hideLoading();
                },
                function(error){
                    var msg = "Data gagal diambil.";
                    var promise = $rootScope.hideLoading();
                    promise.then(function(){
                        var alertPopup = $ionicPopup.alert({
                            title: 'Alert',
                            template: msg
                        });
                        alertPopup.then(function(){
                        });
                    });
                });
        }


        $scope.setMode = function(item){
            $scope.selected = item;
        }

        $scope.isActive = function(item){
            return $scope.selected === item;
        }

        $scope.loadData();
    })
    .controller('PlaylistCtrl', function($scope, $stateParams, $rootScope) {
});
